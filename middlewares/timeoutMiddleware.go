package middlewares

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

func TimeOutMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestId := uuid.New().String()
		logger.Info("New request", zap.String("request_id", requestId), zap.String("path", c.Request.URL.Path))

		ctx := context.WithValue(c.Request.Context(), "RequestID", requestId)
		ctx, cancel := context.WithTimeout(ctx, 15*time.Second)
		defer cancel()

		done := make(chan struct{})
		defer close(done)

		var errType string

		go func() {
			defer func() {
				if err := recover(); err != nil {
					switch err.(type) {
					case string:
						errType = err.(string)
					default:
						errType = "Unknown Error"
					}
					c.Set("ErrorType", errType)
					logger.Error("Request panic", zap.String("request_id", requestId), zap.String("error_type", errType))
				}
			}()

			c.Request = c.Request.WithContext(ctx)
			c.Next()
			done <- struct{}{}
		}()

		select {
		case <-done:
			logger.Info("Request completed", zap.String("request_id", requestId), zap.String("path", c.Request.URL.Path))
			return
		case <-ctx.Done():
			if errType != "" {
				logger.Error("Request timed out with error", zap.String("request_id", requestId), zap.String("error_type", errType))
				c.String(http.StatusInternalServerError, errType)
			} else {
				logger.Warn("Request timed out", zap.String("request_id", requestId), zap.String("path", c.Request.URL.Path))
				c.String(http.StatusGatewayTimeout, "Request timed out")
			}
			c.Abort()
			return
		}
	}
}
