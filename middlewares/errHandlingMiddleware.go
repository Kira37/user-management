package middlewares

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-management/namespaces"
	"go.uber.org/zap"
)

func ErrorMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if len(c.Errors) > 0 {
				err := c.Errors.Last().Err
				if err != nil {
					var statusCode int
					if errorx.IsOfType(err, namespaces.UnableToSaveError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.UnableToSaveError]
					} else if errorx.IsOfType(err, namespaces.UnableToFindResourceError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.UnableToFindResourceError]
					} else if errorx.IsOfType(err, namespaces.UnableToReadError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.UnableToReadError]
					} else if errorx.IsOfType(err, namespaces.UnauthorizedError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.UnauthorizedError]
					} else if errorx.IsOfType(err, namespaces.BindError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.BindError]
					} else if errorx.IsOfType(err, namespaces.DatabaseError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.DatabaseError]
					} else if errorx.IsOfType(err, namespaces.ValidateError) {
						statusCode = namespaces.ErrorStatusMap[namespaces.ValidateError]
					} else {
						statusCode = http.StatusInternalServerError
					}

					logger.Error("Request error", zap.Error(err))
					c.JSON(statusCode, gin.H{"error": err.Error()})
					c.Abort()
				}
			}
		}()
		c.Next()
	}
}
