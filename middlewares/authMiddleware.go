package middlewares

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-management/handlers"
	"go.uber.org/zap"
)

func extractUserIDFromToken(c *gin.Context, logger *zap.Logger) (string, error) {
	tokenString, err := c.Cookie("token")
	if err != nil {
		logger.Error("Failed to get token from cookie", zap.Error(err))
		return "", err
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return []byte(handlers.JWTKey), nil
	})
	if err != nil {
		logger.Error("Token parsing error", zap.Error(err))
		return "", fmt.Errorf("token parsing error: %v", err)
	}

	if !token.Valid {
		logger.Warn("Invalid token")
		return "", errors.New("token is invalid")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		logger.Error("Failed to extract claims from token")
		return "", errors.New("unable to extract claims: claims are not of type jwt.MapClaims")
	}

	userID, ok := claims["user_id"].(string)
	if !ok {
		logger.Error("Failed to extract user ID from token")
		return "", errors.New("unable to extract user ID: UserID claim not found or not a string")
	}
	return userID, nil
}

func AuthMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID, err := extractUserIDFromToken(c, logger)
		if err != nil {
			if errors.Is(err, http.ErrNoCookie) {
				logger.Warn("Unauthorized access: No cookie found", zap.Error(err))
				c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
				c.Abort()
				return
			}
			logger.Warn("Unauthorized access", zap.Error(err))
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}

		c.Set("UserID", userID)
		c.Next()
	}
}
