package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-management/db"
	"gitlab.com/Kira37/user-management/handlers"
	"gitlab.com/Kira37/user-management/middlewares"
	"gitlab.com/Kira37/user-management/utils"
	"go.uber.org/zap"
)

func init() {
	utils.InitLogger()
	defer utils.Logger.Sync()

	db.LoadEnv()
	db.ConnectDB()
	db.CreateTable()
	jwtKey := os.Getenv("JWT_KEY")
	if jwtKey == "" {
		utils.Logger.Fatal("JWT_KEY not found in environment variables")
		return
	}

	handlers.JWTKey = []byte(jwtKey)
}

func main() {
	userRepo := db.NewUserRepo(db.DB)
	userHandler := handlers.NewUserHandler(userRepo, utils.Logger)

	router := gin.New()

	router.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		Formatter: func(param gin.LogFormatterParams) string {
			utils.Logger.Info("HTTP Request",
				zap.String("client_ip", param.ClientIP),
				zap.String("method", param.Method),
				zap.String("path", param.Path),
				zap.Int("status", param.StatusCode),
				zap.String("error", param.ErrorMessage),
				zap.String("user_agent", param.Request.UserAgent()),
				zap.Duration("latency", param.Latency),
			)
			return ""
		},
		Output: os.Stdout,
	}))
	router.Use(gin.Recovery())

	router.Use(middlewares.TimeOutMiddleware(utils.Logger))

	router.POST("/register", middlewares.ErrorMiddleware(utils.Logger), userHandler.Register)
	router.POST("/login", middlewares.ErrorMiddleware(utils.Logger), userHandler.Login)
	router.GET("/refresh", middlewares.ErrorMiddleware(utils.Logger), userHandler.RefreshToken)

	authGroup := router.Group("/")
	authGroup.Use(middlewares.AuthMiddleware(utils.Logger))
	authGroup.Use(middlewares.ErrorMiddleware(utils.Logger))
	{
		authGroup.GET("/users", userHandler.ListUsers)
		authGroup.POST("/upload", userHandler.Upload)
		authGroup.Static("/images", "./uploads")
	}

	utils.Logger.Info("Starting server on :8080")
	router.Run(":8080")
}
