package handlers

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-management/db"
	"gitlab.com/Kira37/user-management/models"
	"gitlab.com/Kira37/user-management/namespaces"
	"gitlab.com/Kira37/user-management/utils"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

type UserHandler struct {
	userRepository db.UserRepository
	logger         *zap.Logger
}

func NewUserHandler(repo db.UserRepository, logger *zap.Logger) *UserHandler {
	return &UserHandler{
		userRepository: repo,
		logger:         logger,
	}
}

var JWTKey []byte

func (h *UserHandler) Register(c *gin.Context) {
	ctx := c.Request.Context()
	requestID := ctx.Value("RequestID").(string)

	var userField models.User
	if err := c.ShouldBindJSON(&userField); err != nil {
		h.logger.Error("Error parsing request body", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.BindError.New("error parsing request body"), "invalid request format"))
		return
	}

	if err := userField.Validate(); err != nil {
		h.logger.Error("Validation failed", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.ValidateError.New("validation failed"), err.Error()))
		return
	}

	user, err := h.userRepository.FindUserByUsername(ctx, userField.Username)
	if user != nil {
		h.logger.Warn("Duplicate username", zap.String("request_id", requestID), zap.String("username", userField.Username))
		c.Error(errorx.Decorate(namespaces.UnableToSaveError.New("username already exists"), err.Error()))
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userField.Password), bcrypt.DefaultCost)
	if err != nil {
		h.logger.Error("Password hashing failed", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToSaveError.New("password hashing failed"), "bcrypt error"))
		return
	}

	userData := models.User{
		ID:       userField.ID,
		Username: userField.Username,
		Password: string(hashedPassword),
		Email:    userField.Email,
		Phone:    userField.Phone,
		Address:  userField.Address,
	}

	err = h.userRepository.CreateUser(ctx, &userData)
	if err != nil {
		h.logger.Error("Unable to save user data", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToSaveError.New("unable to save user data"), err.Error()))
		return
	}

	response := models.Response{
		MetaData: make(map[string]interface{}),
		Data:     userData,
	}

	c.Set("response", response)

	utils.MetaDataHandler(c)
	h.logger.Info("User registered successfully", zap.String("request_id", requestID), zap.String("username", userField.Username))
}

func (h *UserHandler) Login(c *gin.Context) {
	ctx := c.Request.Context()
	requestID := ctx.Value("RequestID").(string)

	var userField models.User
	if err := c.BindJSON(&userField); err != nil {
		h.logger.Error("Error parsing request body", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.BindError.New("error parsing request body"), "invalid request format"))
		return
	}

	user, err := h.userRepository.FindUserByUsername(ctx, userField.Username)
	if (user == nil) || (bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userField.Password)) != nil) {
		h.logger.Warn("Invalid username or password", zap.String("request_id", requestID), zap.String("username", userField.Username))
		c.Error(errorx.Decorate(namespaces.UnauthorizedError.New("invalid username or password"), err.Error()))
		return
	}

	generateToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id":  user.ID,
		"username": user.Username,
		"exp":      time.Now().Add(time.Hour).Unix(),
	})

	tokenString, err := generateToken.SignedString(JWTKey)
	if err != nil {
		h.logger.Error("Token generation failed", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToReadError.New("token generation failed"), err.Error()))
		return
	}

	c.SetCookie("token", tokenString, int(time.Hour.Seconds()), "/", "", false, true)
	h.logger.Info("User logged in successfully", zap.String("request_id", requestID), zap.String("username", userField.Username))
	c.JSON(http.StatusOK, gin.H{"message": "user logged in successfully"})
}

func (h *UserHandler) RefreshToken(c *gin.Context) {
	requestID := c.Request.Context().Value("RequestID").(string)

	tokenCookie, err := c.Cookie("token")
	if err != nil {
		h.logger.Error("Token cookie not found", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToReadError.New("token cookie not found"), err.Error()))
		return
	}

	tokenString := tokenCookie
	token, err := jwt.ParseWithClaims(tokenString, jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		return JWTKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			h.logger.Warn("Invalid token signature", zap.String("request_id", requestID), zap.Error(err))
			c.Error(errorx.Decorate(namespaces.UnauthorizedError.New("invalid token signature"), "JWT error"))
			return
		}
		h.logger.Error("Token parsing failed", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToFindResourceError.New("token parsing failed"), err.Error()))
		return
	}

	newClaims := token.Claims.(jwt.MapClaims)
	newClaims["exp"] = time.Now().Add(time.Hour).Unix()
	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, newClaims)
	tokenString, err = newToken.SignedString(JWTKey)
	if err != nil {
		h.logger.Error("Token signing failed", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToReadError.New("token signing failed"), err.Error()))
		return
	}

	c.SetCookie("token", tokenString, int(time.Hour.Seconds()), "/", "", false, true)
	h.logger.Info("Token refreshed successfully", zap.String("request_id", requestID))
	c.JSON(http.StatusOK, gin.H{"message": "token refreshed successfully"})
}
