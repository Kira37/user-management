package handlers

import (
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-management/models"
	"gitlab.com/Kira37/user-management/namespaces"
	"gitlab.com/Kira37/user-management/utils"
	"go.uber.org/zap"
)

func (h *UserHandler) ListUsers(c *gin.Context) {
	ctx := c.Request.Context()
	requestID := ctx.Value("RequestID").(string)

	params := utils.ParsePaginationParams(c)
	offset := (params.Page - 1) * params.PerPage

	users, err := h.userRepository.GetUsers(ctx, offset, params.PerPage)
	if err != nil {
		h.logger.Error("Failed to fetch users", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.DatabaseError.New("failed to fetch users"), err.Error()))
		return
	}

	if len(users) == 0 {
		h.logger.Warn("No users found", zap.String("request_id", requestID))
		c.Error(errorx.Decorate(namespaces.UnableToFindResourceError.New("no users found"), "empty result set"))
		return
	}

	response := models.Response{
		MetaData: make(map[string]interface{}),
		Data:     users,
	}

	response.MetaData["Page"] = params.Page
	response.MetaData["PerPage"] = params.PerPage

	c.Set("response", response)

	utils.MetaDataHandler(c)
	h.logger.Info("Fetched users successfully", zap.String("request_id", requestID), zap.Int("user_count", len(users)))
}

func (h *UserHandler) Upload(c *gin.Context) {
	requestID := c.Request.Context().Value("RequestID").(string)

	const defaultMaxFileSize = 32 << 20
	c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, defaultMaxFileSize)

	var upload models.ImageUpload
	if err := c.ShouldBind(&upload); err != nil {
		h.logger.Error("Error binding image data", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.BindError.New("error binding image data"), "binding error"))
		return
	}

	file := upload.Image

	contentType := file.Header.Get("Content-Type")
	if !strings.HasPrefix(contentType, "image/") {
		h.logger.Warn("Invalid file type", zap.String("request_id", requestID), zap.String("content_type", contentType))
		c.Error(errorx.Decorate(namespaces.UnableToReadError.New("only image files are allowed"), "invalid file type"))
		return
	}

	err := os.MkdirAll("./uploads", 0755)
	if err != nil {
		h.logger.Error("Error creating directory", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToSaveError.New("error creating directory"), err.Error()))
		return
	}

	err = c.SaveUploadedFile(file, "./uploads/"+file.Filename)
	if err != nil {
		h.logger.Error("Failed to save file", zap.String("request_id", requestID), zap.Error(err))
		c.Error(errorx.Decorate(namespaces.UnableToSaveError.New("failed to save file"), err.Error()))
		return
	}

	response := models.Response{
		MetaData: make(map[string]interface{}),
		Data:     "uploads/" + file.Filename,
	}

	c.Set("response", response)
	utils.MetaDataHandler(c)
	h.logger.Info("File uploaded successfully", zap.String("request_id", requestID), zap.String("file_name", file.Filename))
}
