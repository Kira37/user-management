package namespaces

import (
	"net/http"

	"github.com/joomcode/errorx"
)

var errorNamespace = errorx.NewNamespace("namespaces")

var (
	UnableToSaveError         = errorNamespace.NewType("unable_to_save")
	UnableToFindResourceError = errorNamespace.NewType("unable_to_find_resource")
	UnableToReadError         = errorNamespace.NewType("unable_to_read")
	UnauthorizedError         = errorNamespace.NewType("unauthorized")
	BindError                 = errorNamespace.NewType("bind_error")
	ValidateError             = errorNamespace.NewType("validate_error")
	DatabaseError             = errorNamespace.NewType("database_error")
)

var ErrorStatusMap = map[*errorx.Type]int{
	UnableToSaveError:         http.StatusInternalServerError,
	UnableToFindResourceError: http.StatusNotFound,
	UnableToReadError:         http.StatusInternalServerError,
	UnauthorizedError:         http.StatusUnauthorized,
	BindError:                 http.StatusBadRequest,
	ValidateError:             http.StatusUnprocessableEntity,
	DatabaseError:             http.StatusInternalServerError,
}
