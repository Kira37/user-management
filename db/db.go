package db

import (
	"context"
	"database/sql"

	"gitlab.com/Kira37/user-management/models"
)

type UserRepository interface {
	CreateUser(ctx context.Context, user *models.User) error
	FindUserByUsername(ctx context.Context, username string) (*models.User, error)
	GetUsers(ctx context.Context, offset, limit int) ([]models.User, error)
}

type UserRepo struct {
	DB *sql.DB
}

func NewUserRepo(db *sql.DB) UserRepository {
	return &UserRepo{DB: db}
}

func (repo *UserRepo) CreateUser(ctx context.Context, user *models.User) error {
	query := "INSERT INTO users (id, username, password, email, phone, address) VALUES (?, ?, ?, ?, ?, ?)"
	result, err := repo.DB.ExecContext(ctx, query, user.ID, user.Username, user.Password, user.Email, user.Phone, user.Address)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return err
	}

	return nil
}

func (repo *UserRepo) FindUserByUsername(ctx context.Context, username string) (*models.User, error) {
	user := &models.User{}
	query := "SELECT id, username, password, email, phone, address FROM users WHERE username = ?"
	row := repo.DB.QueryRowContext(ctx, query, username)
	err := row.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Phone, &user.Address)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return user, nil
}

func (repo *UserRepo) GetUsers(ctx context.Context, offset, limit int) ([]models.User, error) {
	var users []models.User
	query := "SELECT id, username, password, email, phone, address FROM users LIMIT ? OFFSET ?"
	rows, err := repo.DB.QueryContext(ctx, query, limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var user models.User
		err := rows.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Phone, &user.Address)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return users, nil
}
