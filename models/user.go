package models

import (
	"encoding/json"
	"regexp"
	"unicode"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

type PhoneNumber string

func (p *PhoneNumber) UnmarshalJSON(data []byte) error {
	var phoneNumber string
	if err := json.Unmarshal(data, &phoneNumber); err != nil {
		return err
	}

	var re = []*regexp.Regexp{
		regexp.MustCompile(`^(\+251|251|0)?([79][0-9]{8})$`),
		regexp.MustCompile(`^([79][0-9]{8})$`),
	}

	for _, r := range re {
		if match := r.FindStringSubmatch(phoneNumber); match != nil {
			*p = PhoneNumber("251" + match[2])
			return nil
		}
	}

	return nil
}

type User struct {
	ID       string      `json:"id,omitempty"`
	Username string      `json:"username,omitempty"`
	Password string      `json:"password,omitempty"`
	Email    string      `json:"email,omitempty"`
	Phone    PhoneNumber `json:"phone,omitempty"`
	Address  string      `json:"address,omitempty"`
}

func (u User) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Length(2, 20)),
		validation.Field(&u.Password, validation.Length(4, 40)),
		validation.Field(&u.Email, is.Email),
		validation.Field(&u.Phone, validation.By(ValidPhoneNumber)),
		validation.Field(&u.Address, validation.By(NoNumbers)),
	)
}

func NoNumbers(value interface{}) error {
	str, _ := value.(string)
	for _, char := range str {
		if unicode.IsDigit(char) {
			return validation.NewError("validation_no_numbers", "must not contain numbers")
		}
	}
	return nil
}

func ValidPhoneNumber(value interface{}) error {
	phone, ok := value.(PhoneNumber)
	if !ok {
		return validation.NewError("validation_invalid_phone", "invalid phone number type")
	}
	re := regexp.MustCompile(`^(\+251|251|0)?([79][0-9]{8})$`)
	if !re.MatchString(string(phone)) {
		return validation.NewError("validation_invalid_phone", "invalid phone number format")
	}
	return nil
}
