package models

type Response struct {
	MetaData map[string]interface{} `json:"meta_data"`
	Data     interface{}            `json:"data"`
}
