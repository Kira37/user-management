package models

import "mime/multipart"

type ImageUpload struct {
	Image *multipart.FileHeader `form:"image" binding:"required"`
}
