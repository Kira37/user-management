package utils

import "go.uber.org/zap"

var Logger *zap.Logger

func InitLogger() {
	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.InfoLevel),
		Development:       false,
		Encoding:          "json",
		EncoderConfig:     zap.NewProductionEncoderConfig(),
		OutputPaths:       []string{"stdout", "api.log"},
		ErrorOutputPaths:  []string{"stderr"},
		InitialFields:     map[string]interface{}{"app": "user-management-api"},
		DisableStacktrace: true,
	}

	var err error
	if Logger, err = config.Build(); err != nil {
		panic(err)
	}
}
