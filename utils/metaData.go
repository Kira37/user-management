package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-management/models"
)

func MetaDataHandler(c *gin.Context) {
	response, exists := c.Get("response")
	if !exists {
		response = models.Response{}
	}

	resp := response.(models.Response)

	if len(resp.MetaData) == 0 {
		resp.MetaData = map[string]interface{}{
			"status":  "success",
			"message": "Request processed successfully",
		}
	}
	c.JSON(http.StatusOK, resp)
}
