package utils

import (
	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-management/namespaces"
)

type PaginationParams struct {
	Page    int `form:"page" binding:"gte=0"`
	PerPage int `form:"per_page" binding:"gte=0"`
}

const (
	DefaultPage    = 1
	DefaultPerPage = 10
)

func ParsePaginationParams(c *gin.Context) PaginationParams {
	var params PaginationParams

	if err := c.ShouldBindQuery(&params); err != nil {
		c.Error(errorx.Decorate(namespaces.UnableToReadError.New("error binding a query"), err.Error()))
		return params
	}

	if params.Page == 0 {
		params.Page = DefaultPage
	}

	if params.PerPage == 0 {
		params.PerPage = DefaultPerPage
	}

	return params
}
